def primesGenerator(number):            #this is a function that generates prime numbers to be used for the factorization

    primesList = [2]                    #list to hold primes
    counter = 3                         #counter used to generate numbers

    if int(number) < 2:                 #if the number is smaller than 2 we return None
        return None

    while True:                         #run until we find all primes smaller than the given number
        for i in range(len(primesList)):
            if counter % primesList[i] == 0:        #check to see if the number is divisible by any previous prime
                break
            if i == len(primesList) - 1:            #if not we add it to the list
                primesList.append(counter)
#                print(primesList[i])
                break
        counter += 1                                #we need to increase counter to check for the next number
        if primesList[len(primesList) - 1] >= int(number):      #when we find all of them:
            primesList.pop()                                        #we get rid of the last one because it is greater than the given number
            primesList.append(number)                               #we append the number itself in case we need to divide by it
            return primesList

number = input('Please enter a number to be factorized\n')      #get input

primes = primesGenerator(number)        #call function to generate our needed primes
print(primes)

output = []         #output to hold factors

while int(number) > 1:                              #need to cycle until the number is equal to 1
    for i in range(len(primes)):                    #cycle throw all the primes to find one that our number is divisible by
        if int(number) % int(primes[i]) == 0:       #when we find the number we divide by it and add it to the output
            number = int(number) / int(primes[i])
            output.append(primes[i])
            break                                   #then we break to start from the beginning of the primes

print(output)
