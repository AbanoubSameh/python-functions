try:                                    #try to open file
    primes = open('primes.txt', 'w')
except:
    print('Could not open file')

length = input("How many primes do you want to generate?\n")        #ask user how many numbers to be generated

primesList = [2]            #list to contian numbers in order to be able to compare new ones to them
counter = 3                 #counter is used to generate more numbers

primes.write("2" + "\n")    #write 2 to file because we already have it

while True:                 #loop to add more numbers to list
    for i in range(len(primesList)):                #compare the new numbers to all the previous numbers in the list
        if counter % primesList[i] == 0:            #if it is divisible by any of them break
            break
        if i == len(primesList) - 1:                #if we reach the end and it is not divisible by any of them:
            primesList.append(counter)              #add it to the end of the list (that's we python is chosen for that kind of work because it is flexible and you can add items to list easily)
            print(primesList[i])                    #print the number
            primes.write(str(counter) + "\n")       #add number to file
            break                                   #lastly break
    counter += 1                            #now increament counter to test the next number
    if len(primesList) == int(length):      #when we reach our target we close the file and exit
        primes.close()
        exit()

