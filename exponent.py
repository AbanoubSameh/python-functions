import sys
from pascal_triangle import pascal

def char(n):
    if n - 1 == 1:
        return ''
    x = str(n - 1)
    out = ''
    for i in range(0, len(x)):
        if x[i] == '0': out += u"\u2070"
        elif x[i] == '1': out += u"\u00B9"
        elif x[i] == '2': out += u"\u00B2"
        elif x[i] == '3': out += u"\u00B3"
        elif x[i] == '4': out += u"\u2074"
        elif x[i] == '5': out += u"\u2075"
        elif x[i] == '6': out += u"\u2076"
        elif x[i] == '7': out += u"\u2077"
        elif x[i] == '8': out += u"\u2078"
        elif x[i] == '9': out += u"\u2079"
#        elif x == u"\u2070": return 0
#        elif x == u"\u00B9": return 1
#        elif x == u"\u00B2": return 2
#        elif x == u"\u00B3": return 3
#        elif x == u"\u2074": return 4
#        elif x == u"\u2075": return 5
#        elif x == u"\u2076": return 6
#        elif x == u"\u2077": return 7
#        elif x == u"\u2078": return 8
#        elif x == u"\u2079": return 9
    return out

def check(x):
    if x != 1:
        return x
    else:
        return ''

string1 = input('Please enter first symbol')
string2 = input('Please enter second symble')
power = int(input('Please enter an exponent')) + 1

if power == 1:
    print(1)
    sys,exit()

for i in range(0, power):
    if i == 0:
        print(str(check(pascal(power).nums[i])) + string1 + char(power - i) + '+', end="")
    elif i == power - 1:
        print(str(check(pascal(power).nums[i])) + string2 + char(power), end="")
    else:
        print(str(check(pascal(power).nums[i])) + string1 + char(power - i) + string2 + char(i + 1) + '+', end="")

print()
