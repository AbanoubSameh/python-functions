try:
    fib = open('fibonacci.txt', 'w')        #try to open file
except:
    print('Could not open file')            #error message in case of failure

length = input("How many numbers do you want to generate?\n")           #ask user how many numbers to be generated

num1 = 0        #first number
num2 = 1        #second number
nextNum = 0     #place for another number to be generated

if int(length) > 0:
    print(num1)     #print first and second numbers because we already have those
    print(num2)
else:
    print("number has to be greater than 0")

fib.write(str(num1) + "\n")     #add them to file
fib.write(str(num2) + "\n")

for i in range(0, int(length) - 2):         #loop until we reach our target
    nextNum = int(num1) + int(num2)         #calculate next number base on the latest two that we have
    num1 = num2                             #move the second number to the first
    num2 = nextNum                          #move the newly generated number to the second
    print(nextNum)                          #print it and add it to file
    fib.write(str(nextNum) + "\n")

fib.close()         #close the file after we are done