import time

def print_diff(input):
    global num1

    num2 = int(input)
    print(abs(num2 - num1))
    num1 = num2

with open("testa9") as f:
    content = f.read().splitlines()

num1 = int(content[0])

for i in content[1:]:
    print_diff(i)
    #time.sleep(1)
