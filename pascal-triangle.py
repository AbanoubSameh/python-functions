class Row:              #class Row contains the row length and the numbers in that row
    number = None
    nums = []

    def __init__(self, number):
        self.number = number
        self.nums = [0] * number        #nums has a length equal to that of the row

row1 = Row(1)                           #first row has only one number which is 1
row1.nums.append(1)

rows = input("Please enter number of rows to be generated\n")       #get desired length

if int(rows) < 1:                       #check that it is greater than 0
    print("number has to be at least 1")
else:
    print(1, "\n")

for i in range(2, int(rows) + 1):       #loop until desired length is reached

    row2 = Row(i)                       #create a new row

    for j in range(0, row2.number):     #loop to generate the numbers in each row one by one

        if j == 0 or j == row2.number - 1:      #if it is the first of last number in that row it is automatically equal to 1
#            print(j, " run: ", 1)
            row2.nums[j] = 1
        else:                                   #else we add the number in the same place in the previous row and the one before it
#            print(j, " run: ", row1.nums[j - 1] + row1.nums[j])
            row2.nums[j] = row1.nums[j - 1] + row1.nums[j]

    for j in range(0, row2.number):     #simple loop used to print the row
        if j != row2.number - 1:
            print(row2.nums[j], " ", end="")
        else:
            print(row2.nums[j], end="")

#    print(row2.nums)

    print("\n")         #print new lines between rows
    row1 = row2         #move the second row to the first to free the second to be generated all over again
